import re
import os
import sys
from collections import OrderedDict

class IPAddressManipulator:
    
    def readInputFile(self):
        input_file_name = "input.txt"
        output_file_name = "results.txt"
        input_file = open("ignore_resources/" + input_file_name, 'r+')
        for line in input_file:
            try:
                line = line[:-1]
                print("Testing the following ip: " + line)
                command = "(nmap -sP -PS22, 3389 " + line + ") >> ignore_resources/" + output_file_name
                os.system(command)
            except KeyboardInterrupt:
                exitProgram()

    def getIPsFromFile(self):
        #input_file = "ignore_resources/results.txt"
        #output_file = "ignore_resources/parsed_ips.txt"

        print("Finding all ip addresses...")

        command = "grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' ignore_resources/parsed_results.txt | uniq > ignore_resources/parsed_ips.txt"
        os.system(command)        

    def clean(self):
        output_file_name = "ignore_resources/parsed_results.txt"
        cleaned_file = open(output_file_name, "w+")
	#Make sure parsed_results file is emptied after every run"
	cleaned_file.write("")       
 
        content = []
        print("Starting to clean...")
        with open("ignore_resources/results.txt") as file:
            content = file.readlines()

        self.cleanDetailsFile(content)
        
        print("Done cleaning!")

    def cleanDetailsFile(self, content):
        line_number = 0
        line_start = 0
        for line in content:
            #If string is found, we are starting a new search
            if line.find("Starting Nmap") != -1:
                line_start = line_number
            elif line.find("Nmap done:") != -1:
                #Check if there are any active hosts
                if self.hasActiveHosts(line):
                    #Output from lines line_start to
                    #the current line number in 'content' variable
                    self.outputCleanedContents(content, line_start, line_number)
            line_number += 1
               
    def hasActiveHosts(self, line):
        start = line.find("Nmap done:")
        end = line.find("IP addresses")
        str_to_search = line[start:end]
        result = re.search("[1-9]", str_to_search)
        if result== None:
           return False
        return True

    def outputCleanedContents(self, content, line_start, end_line):
        output_file_name = "ignore_resources/parsed_results.txt"
        cleaned_file = open(output_file_name, "aw+")
        for index in range(line_start, end_line):
            cleaned_file.write(content[index][:-1] + "\n")
            

def main():
    choice = getUserChoice()
    executeUserChoice(choice)

def getUserChoice():
    print("Welcome, type the number of the action you wish to perform: ")
    print("1) Scan IP addresses from a file full of line-separated ip addresses")
    print("2) Retrieve all IP addresses found in a file")
    print("3) Clean up a file that is cluttered with '0 hosts found'")
    print("-1) Anything else to exit program")
    try:
        choice = int(raw_input("Your choice: "))
    except KeyboardInterrupt:
        exitProgram()
    return choice

def executeUserChoice(choice):
    ip_manipulator = IPAddressManipulator()
    if choice == 1: 
        ip_manipulator.readInputFile()
    elif choice == 2:
        ip_manipulator.getIPsFromFile()
    elif choice == 3:
        ip_manipulator.clean()
    else:
        exitProgram()

def exitProgram():
    print("Bye bye!")
    sys.exit(0)

main()
